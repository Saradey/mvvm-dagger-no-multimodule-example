package com.evgeny.goncharov.newsgameupdates.repository

import com.google.gson.annotations.SerializedName

class BaseResponse<T> {

    @SerializedName("data")
    var data: T? = null

    @SerializedName("success")
    var success: Boolean? = null

    @SerializedName("error")
    var error: BaseError? = null

}