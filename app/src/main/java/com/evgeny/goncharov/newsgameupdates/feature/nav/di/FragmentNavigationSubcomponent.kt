package com.evgeny.goncharov.newsgameupdates.feature.nav.di

import com.evgeny.goncharov.newsgameupdates.di.scopes.NavigationFragmentScope
import com.evgeny.goncharov.newsgameupdates.feature.nav.di.modules.NavigationBindsModule
import com.evgeny.goncharov.newsgameupdates.feature.nav.di.modules.NavigationModule
import com.evgeny.goncharov.newsgameupdates.feature.nav.di.modules.NavigationModuleCreateSubcomponents
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationViewImpl
import com.evgeny.goncharov.newsgameupdates.feature.nav.view.model.NavigationViewModel
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.TopThreeGamesFragment
import dagger.BindsInstance
import dagger.Subcomponent


@NavigationFragmentScope
@Subcomponent(
    modules = [
        NavigationBindsModule::class,
        NavigationModule::class,
        NavigationModuleCreateSubcomponents::class
    ]
)
interface FragmentNavigationSubcomponent {

    fun inject(viewModel: NavigationViewModel)

    fun inject(view: NavigationViewImpl)

    fun inject(fragment: TopThreeGamesFragment)

    @Subcomponent.Factory
    interface Factory {
        fun plus(
            @BindsInstance fragment: NavigationFragment
        ): FragmentNavigationSubcomponent
    }

}