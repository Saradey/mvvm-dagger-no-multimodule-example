package com.evgeny.goncharov.newsgameupdates.managers.impl

import android.content.Context
import android.net.ConnectivityManager
import com.evgeny.goncharov.newsgameupdates.managers.INetworkManager
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_APPLICATION_TAG
import javax.inject.Inject
import javax.inject.Named


class NetworkManagerImpl @Inject constructor(
    @Named(INJECT_APPLICATION_TAG) val context: Context
) : INetworkManager {


    override fun checkInternet(): Boolean {
        val cm =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }


}