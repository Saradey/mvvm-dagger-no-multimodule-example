package com.evgeny.goncharov.newsgameupdates

import android.app.Application
import com.evgeny.goncharov.newsgameupdates.di.components.AppComponent
import com.evgeny.goncharov.newsgameupdates.di.components.DaggerAppComponent

class App : Application() {


    companion object {
        lateinit var appComponent: AppComponent
    }


    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.factory()
            .plus(app = this)
    }


}