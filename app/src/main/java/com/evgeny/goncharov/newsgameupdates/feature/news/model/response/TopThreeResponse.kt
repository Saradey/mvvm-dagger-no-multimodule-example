package com.evgeny.goncharov.newsgameupdates.feature.news.model.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
class TopThreeResponse {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @SerializedName("nameGame")
    var nameGame: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("dateUpdate")
    var dateUpdate: Date? = null

}