package com.evgeny.goncharov.newsgameupdates.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.evgeny.goncharov.newsgameupdates.db.type.converters.TypeConverterDate
import com.evgeny.goncharov.newsgameupdates.feature.news.db.TopThreeDAO
import com.evgeny.goncharov.newsgameupdates.feature.news.model.response.TopThreeResponse
import com.evgeny.goncharov.newsgameupdates.utils.DATABASE_VERSION_1

@Database(entities = [TopThreeResponse::class], version = DATABASE_VERSION_1)
@TypeConverters(value = [TypeConverterDate::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun createTopThreeDAO(): TopThreeDAO

}