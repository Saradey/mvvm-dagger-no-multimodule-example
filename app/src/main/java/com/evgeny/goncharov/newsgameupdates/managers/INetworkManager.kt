package com.evgeny.goncharov.newsgameupdates.managers

interface INetworkManager {

    fun checkInternet() : Boolean

}