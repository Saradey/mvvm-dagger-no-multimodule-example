package com.evgeny.goncharov.newsgameupdates.feature.nav.di.modules

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.evgeny.goncharov.newsgameupdates.di.scopes.NavigationFragmentScope
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.feature.nav.view.model.INavigationViewModel
import com.evgeny.goncharov.newsgameupdates.feature.nav.view.model.NavigationViewModel
import dagger.Module
import dagger.Provides

@Module
class NavigationModule {

    @Provides
    fun provideViewModel(fragmentNav: NavigationFragment): INavigationViewModel =
        ViewModelProviders.of(fragmentNav).get(NavigationViewModel::class.java)

    @Provides
    @NavigationFragmentScope
    fun provideNavigationLiveDataToolbar(): MutableLiveData<Int> =
        MutableLiveData()

}