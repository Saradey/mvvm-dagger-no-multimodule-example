package com.evgeny.goncharov.newsgameupdates.db.type.converters

import androidx.room.TypeConverter
import com.evgeny.goncharov.newsgameupdates.App
import com.evgeny.goncharov.newsgameupdates.utils.DateFormatter
import java.util.*
import javax.inject.Inject

class TypeConverterDate {

    @Inject
    lateinit var dateFormatter: DateFormatter

    init {
        App.appComponent.inject(this)
    }

    @TypeConverter
    fun fromData(date: Date): String {
        return dateFormatter.sdfFullAllInformationDate.format(date)
    }

    @TypeConverter
    fun toDate(date: String): Date {
        return dateFormatter.sdfFullAllInformationDate.parse(date)!!
    }

}