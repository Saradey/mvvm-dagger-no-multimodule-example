package com.evgeny.goncharov.newsgameupdates.utils.navigation

sealed class Destination {

    object BackPressed : Destination()

    object SplashScreen : Destination()

    object TopThreeGamesScreen : Destination()

    object NavigationBottomScreen : Destination()

}