package com.evgeny.goncharov.newsgameupdates.feature.nav.ui

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.base.BaseView
import com.evgeny.goncharov.newsgameupdates.feature.nav.view.model.NavigationViewModel
import kotlinx.android.synthetic.main.fragment_navigation.view.*
import javax.inject.Inject

class NavigationViewImpl : BaseView(), INavigationView {

    init {
        NavigationFragment.component.inject(this)
    }

    @Inject
    lateinit var viewModel: NavigationViewModel

    @Inject
    lateinit var liveDataToolbar: MutableLiveData<Int>


    override fun attach(view: View?) {
        container = view
    }


    override fun init() {
        initUi()
        initLiveData()
    }


    override fun initUi() {
        initBottomNavigation()
        initTopThreeFragment()
    }


    private fun initTopThreeFragment() {
        viewModel.initTopThreeFragment()
    }


    private fun initLiveData() {
        liveDataToolbar.observeForever { idString ->
            container?.toolbarNav?.setTitle(idString)
        }
    }


    private fun initBottomNavigation() {
        container?.btmMainMenu?.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navTopThree -> {
                    viewModel.clickNavBottomTopThree()
                }
                R.id.navLogin -> {
                    viewModel.clickNavBottomLogin()
                }
                R.id.navSettings -> {
                    viewModel.clickNavBottomSettings()
                }
            }
            true
        }
    }


    override fun injectView() {
        //Nothing
    }
}