package com.evgeny.goncharov.newsgameupdates.feature.splash

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.evgeny.goncharov.newsgameupdates.MainActivity
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainRouter
import javax.inject.Inject

class SplashScreenFragment : Fragment() {

    @Inject
    lateinit var router: IMainRouter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.activityComponent.inject(this)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(requireContext())
            .inflate(R.layout.fragment_splash_screen, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        startTimer()
    }


    private fun startTimer() {
        object : CountDownTimer(2000, 1000) {
            override fun onFinish() {
                router.goToTheNavigationBotttomScreen()
            }

            override fun onTick(p0: Long) {}
        }.start()
    }

}