package com.evgeny.goncharov.newsgameupdates.di.modules.app

import com.evgeny.goncharov.newsgameupdates.di.components.ActivitySubcomponent
import dagger.Module

@Module(
    subcomponents = [ActivitySubcomponent::class]
)
interface CreateActivitySubcomponentModule