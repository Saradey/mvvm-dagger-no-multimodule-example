package com.evgeny.goncharov.newsgameupdates.utils.navigation

import com.evgeny.goncharov.newsgameupdates.MainActivity

interface IMainNavigator {

    fun attachActivity(mainActivity: MainActivity)

    fun detachActivity()

    fun goTo(des: Destination)

}