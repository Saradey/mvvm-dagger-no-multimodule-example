package com.evgeny.goncharov.newsgameupdates.feature.news.view.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.evgeny.goncharov.newsgameupdates.feature.news.interactor.ITopThreeGamesInteractor
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.TopThreeGamesFragment
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_MAIN_SCOPE_TAG
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

class TopThreeGamesViewModel : ITopThreeGamesViewModel, ViewModel() {

    @Inject
    lateinit var interactor: ITopThreeGamesInteractor

    @field:[Inject Named(INJECT_MAIN_SCOPE_TAG)]
    lateinit var mainIoCoroutine: CoroutineScope

    @Inject
    lateinit var liveDataTopThreeViewPager: MutableLiveData<List<TopThreeModelView>>


    init {
        TopThreeGamesFragment.component.inject(this)
    }


    override fun initViewState() {
        mainIoCoroutine.launch {
            val models = interactor.initViewState()
            liveDataTopThreeViewPager.postValue(models)
        }
    }
}