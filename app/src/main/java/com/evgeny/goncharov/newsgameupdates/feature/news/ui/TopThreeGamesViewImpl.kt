package com.evgeny.goncharov.newsgameupdates.feature.news.ui

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.evgeny.goncharov.newsgameupdates.base.BaseView
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.adapters.ViewPagerTopThreeAdapter
import com.evgeny.goncharov.newsgameupdates.feature.news.view.model.ITopThreeGamesViewModel
import com.evgeny.goncharov.newsgameupdates.utils.UiEvent
import kotlinx.android.synthetic.main.fragment_top_three_games.view.*
import javax.inject.Inject

class TopThreeGamesViewImpl : BaseView(), ITopThreeGamesView {

    @Inject
    lateinit var viewModel: ITopThreeGamesViewModel

    private lateinit var adapterViewPager: ViewPagerTopThreeAdapter

    @Inject
    lateinit var liveDataTopThreeViewPager: MutableLiveData<List<TopThreeModelView>>

    @Inject
    lateinit var liveDataUiEvents: MutableLiveData<UiEvent>


    override fun injectView() {
        TopThreeGamesFragment.component.inject(this)
    }


    override fun attach(view: View?) {
        container = view
    }


    override fun init() {
        initLiveData()
        initUi()
        initViewState()
    }


    private fun initLiveData() {
        observeFlowModelTpViewPager()
        observeUiEvents()
    }


    private fun observeUiEvents() {
        liveDataUiEvents.observeForever { event ->
            handlingUiEvents(event)
        }
    }


    private fun observeFlowModelTpViewPager() {
        liveDataTopThreeViewPager.observeForever { model ->
            adapterViewPager.listModel = model
            container?.vp2TopThree?.currentItem = 1
        }
    }


    private fun initViewState() {
        viewModel.initViewState()
    }


    override fun initUi() {
        initAdapterAndViewPager()
    }


    private fun initAdapterAndViewPager() {
        adapterViewPager = ViewPagerTopThreeAdapter()
        container?.vp2TopThree?.adapter = adapterViewPager
    }


    private fun handlingUiEvents(event: UiEvent) {
        when (event) {
            UiEvent.EventShowLoader -> showProgress()
            UiEvent.EventHideLoader -> hideProgress()
            UiEvent.EventShowErrorSomethingWrong -> showErrorSomethingWrong()
        }
    }
}