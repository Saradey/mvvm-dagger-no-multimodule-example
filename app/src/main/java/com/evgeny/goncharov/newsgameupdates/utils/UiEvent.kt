package com.evgeny.goncharov.newsgameupdates.utils

sealed class UiEvent {

    object EventShowLoader : UiEvent()

    object EventHideLoader : UiEvent()

    object EventShowErrorSomethingWrong : UiEvent()

}