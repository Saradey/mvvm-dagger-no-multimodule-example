package com.evgeny.goncharov.newsgameupdates.di.modules.app

import com.evgeny.goncharov.newsgameupdates.di.scopes.AppScope
import com.evgeny.goncharov.newsgameupdates.managers.ILanguageManager
import com.evgeny.goncharov.newsgameupdates.utils.DateFormatter
import dagger.Module
import dagger.Provides

@Module
class AppUtilsModule {

    @AppScope
    @Provides
    fun provideDateFormatter(languageManager: ILanguageManager) = DateFormatter(languageManager)


}