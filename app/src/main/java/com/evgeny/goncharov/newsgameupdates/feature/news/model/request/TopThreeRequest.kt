package com.evgeny.goncharov.newsgameupdates.feature.news.model.request

import com.evgeny.goncharov.newsgameupdates.repository.BaseRequest
import com.evgeny.goncharov.newsgameupdates.utils.LANG

class TopThreeRequest(
    private val lang: String
) : BaseRequest() {

    override fun createQueryMap(): Map<String, String> {
        val map = hashMapOf<String, String>()
        map[LANG] = lang
        createDefaultMap(map)
        return map
    }

}