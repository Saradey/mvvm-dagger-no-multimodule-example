package com.evgeny.goncharov.newsgameupdates.base

import android.view.View
import com.evgeny.goncharov.newsgameupdates.R
import kotlinx.android.synthetic.main.fragment_top_three_games.view.*
import kotlinx.android.synthetic.main.include_no_wifi.view.*

abstract class BaseView : IBaseView, IBaseStateErrorView {

    protected var container: View? = null


    override fun showProgress() {
    }


    override fun hideProgress() {
    }


    override fun showErrorSomethingWrong() {
        container?.grpAllContent?.visibility = View.GONE
        container?.grpErrorPlaceHolder?.visibility = View.VISIBLE
    }


    override fun hideErrorSomethingWrong() {
        container?.grpAllContent?.visibility = View.VISIBLE
        container?.grpErrorPlaceHolder?.visibility = View.GONE
    }
}