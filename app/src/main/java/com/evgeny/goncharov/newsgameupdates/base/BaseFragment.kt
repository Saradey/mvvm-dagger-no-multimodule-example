package com.evgeny.goncharov.newsgameupdates.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    abstract fun getLayoutId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(requireContext()).inflate(
            getLayoutId(),
            container, false
        )
        return view
    }

    abstract fun init()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }

    protected abstract fun createView(view: View?)
}