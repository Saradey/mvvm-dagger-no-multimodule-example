package com.evgeny.goncharov.newsgameupdates.repository

import com.evgeny.goncharov.newsgameupdates.utils.API_KEY
import com.evgeny.goncharov.newsgameupdates.utils.API_KEY_VALUE

abstract class BaseRequest {

    abstract fun createQueryMap(): Map<String, String>

    protected fun createDefaultMap(map: HashMap<String, String>) {
        map[API_KEY] = API_KEY_VALUE
    }

}