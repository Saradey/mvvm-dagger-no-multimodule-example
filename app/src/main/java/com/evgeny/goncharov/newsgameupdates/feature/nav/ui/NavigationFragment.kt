package com.evgeny.goncharov.newsgameupdates.feature.nav.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.evgeny.goncharov.newsgameupdates.MainActivity
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.base.BaseFragment
import com.evgeny.goncharov.newsgameupdates.feature.nav.di.FragmentNavigationSubcomponent
import javax.inject.Inject

class NavigationFragment : BaseFragment() {

    companion object {
        lateinit var component: FragmentNavigationSubcomponent
    }

    override fun getLayoutId() = R.layout.fragment_navigation

    @Inject
    lateinit var builderComponent: FragmentNavigationSubcomponent.Factory

    lateinit var view: INavigationView


    override fun init() {
        view.init()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.activityComponent.inject(this)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (savedInstanceState == null) {
            component = builderComponent.plus(this)
        }
        createView(view)
        return view
    }


    override fun createView(view: View?) {
        this.view = NavigationViewImpl()
        this.view.attach(view)
    }
}