package com.evgeny.goncharov.newsgameupdates.feature.news.repository

import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView

interface ITopThreeGamesRepository {

    suspend fun loadFromInternet(request: Map<String, String>)

    suspend fun loadFromDataBase(): List<TopThreeModelView>

}