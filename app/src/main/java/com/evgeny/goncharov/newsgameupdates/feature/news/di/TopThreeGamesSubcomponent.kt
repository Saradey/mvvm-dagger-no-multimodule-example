package com.evgeny.goncharov.newsgameupdates.feature.news.di

import com.evgeny.goncharov.newsgameupdates.di.scopes.FragmentScope
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.ITopThreeGamesView
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.TopThreeGamesFragment
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.TopThreeGamesViewImpl
import com.evgeny.goncharov.newsgameupdates.feature.news.view.model.TopThreeGamesViewModel
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [TopThreeGamesModule::class, TopThreeGamesBindsModule::class])
@FragmentScope
interface TopThreeGamesSubcomponent {

    fun inject(topThreeGamesView: TopThreeGamesViewImpl)

    fun inject(viewModel: TopThreeGamesViewModel)


    @Subcomponent.Factory
    interface Factory {
        fun plus(
            @BindsInstance fragment: TopThreeGamesFragment,
            @BindsInstance view: ITopThreeGamesView
        ): TopThreeGamesSubcomponent
    }


}