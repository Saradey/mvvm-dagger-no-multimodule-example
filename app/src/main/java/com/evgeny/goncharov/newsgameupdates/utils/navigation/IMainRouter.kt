package com.evgeny.goncharov.newsgameupdates.utils.navigation

interface IMainRouter {

    fun startSplashScreen()

    fun goToTheNavigationBotttomScreen()

}