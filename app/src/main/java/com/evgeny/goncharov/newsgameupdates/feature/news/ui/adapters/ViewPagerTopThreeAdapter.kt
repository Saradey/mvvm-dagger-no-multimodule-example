package com.evgeny.goncharov.newsgameupdates.feature.news.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.holders.TopThreeHolder

class ViewPagerTopThreeAdapter : RecyclerView.Adapter<TopThreeHolder>() {

    var listModel = listOf<TopThreeModelView>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopThreeHolder {
        return TopThreeHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.holder_viewpager_top_game,
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return listModel.size
    }


    override fun onBindViewHolder(holder: TopThreeHolder, position: Int) {
        holder.bind(listModel[position])
    }


}