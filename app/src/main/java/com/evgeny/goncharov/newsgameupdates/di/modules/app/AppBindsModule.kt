package com.evgeny.goncharov.newsgameupdates.di.modules.app

import android.content.Context
import com.evgeny.goncharov.newsgameupdates.App
import com.evgeny.goncharov.newsgameupdates.di.scopes.AppScope
import com.evgeny.goncharov.newsgameupdates.managers.ILanguageManager
import com.evgeny.goncharov.newsgameupdates.managers.INetworkManager
import com.evgeny.goncharov.newsgameupdates.managers.impl.LanguageManagerImpl
import com.evgeny.goncharov.newsgameupdates.managers.impl.NetworkManagerImpl
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_APPLICATION_TAG
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainNavigator
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainRouter
import com.evgeny.goncharov.newsgameupdates.utils.navigation.MainNavigatorImpl
import com.evgeny.goncharov.newsgameupdates.utils.navigation.MainRouterImpl
import dagger.Binds
import dagger.Module
import javax.inject.Named

@Module
interface AppBindsModule {

    @Binds
    @Named(INJECT_APPLICATION_TAG)
    fun bindApp(app: App): Context

    @Binds
    @AppScope
    fun bindNavigation(nav: MainNavigatorImpl): IMainNavigator

    @Binds
    @AppScope
    fun bindRouter(router: MainRouterImpl): IMainRouter

    @Binds
    @AppScope
    fun bindNetworkManager(manager: NetworkManagerImpl): INetworkManager

    @Binds
    @AppScope
    fun bindLanguageManager(languageManager: LanguageManagerImpl): ILanguageManager

}