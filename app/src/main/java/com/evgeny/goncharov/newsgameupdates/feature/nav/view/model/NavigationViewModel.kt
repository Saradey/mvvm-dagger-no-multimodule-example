package com.evgeny.goncharov.newsgameupdates.feature.nav.view.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.evgeny.goncharov.newsgameupdates.App
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.feature.nav.navigation.INavNavigation
import com.evgeny.goncharov.newsgameupdates.feature.nav.navigation.INavRouter
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainRouter
import javax.inject.Inject

class NavigationViewModel @Inject constructor() : ViewModel(), INavigationViewModel {

    @Inject
    lateinit var liveDataToolbar: MutableLiveData<Int>

    @Inject
    lateinit var mainRouter: IMainRouter

    @Inject
    lateinit var navRouter: INavRouter


    init {
        NavigationFragment.component.inject(this)
    }


    override fun clickNavBottomTopThree() {
        liveDataToolbar.postValue(R.string.nav_top_three)
        navRouter.goToTheTopThree()
    }


    override fun clickNavBottomLogin() {
        liveDataToolbar.postValue(R.string.nav_auto)
        //TODO переход на фрагмент логина
    }


    override fun clickNavBottomSettings() {
        //TODO переход на фрагмент настроек
    }


    override fun initTopThreeFragment() {
        navRouter.goToTheTopThree()
    }
}