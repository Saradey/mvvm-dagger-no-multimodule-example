package com.evgeny.goncharov.newsgameupdates.di.modules.activity

import android.content.Context
import com.evgeny.goncharov.newsgameupdates.MainActivity
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_ACTIVITY_TAG
import dagger.Binds
import dagger.Module
import javax.inject.Named

@Module
interface ActivityBindsModule {

    @Binds
    @Named(INJECT_ACTIVITY_TAG)
    fun bindMainActivity(mainActivity: MainActivity): Context

}