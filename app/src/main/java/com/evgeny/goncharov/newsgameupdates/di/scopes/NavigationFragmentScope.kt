package com.evgeny.goncharov.newsgameupdates.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class NavigationFragmentScope {
}