package com.evgeny.goncharov.newsgameupdates.repository

import com.google.gson.annotations.SerializedName

class BaseError {

    @SerializedName("code")
    var error: String? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("type")
    var type: String? = null

}