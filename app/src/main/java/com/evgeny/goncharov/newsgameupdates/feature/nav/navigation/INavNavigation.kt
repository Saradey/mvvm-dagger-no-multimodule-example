package com.evgeny.goncharov.newsgameupdates.feature.nav.navigation

import com.evgeny.goncharov.newsgameupdates.utils.navigation.Destination

interface INavNavigation {

    fun goTo(destination: Destination)

}