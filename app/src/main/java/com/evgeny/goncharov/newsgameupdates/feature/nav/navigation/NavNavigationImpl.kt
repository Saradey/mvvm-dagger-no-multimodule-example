package com.evgeny.goncharov.newsgameupdates.feature.nav.navigation

import androidx.fragment.app.Fragment
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.base.BaseFragment
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.TopThreeGamesFragment
import com.evgeny.goncharov.newsgameupdates.utils.navigation.Destination
import javax.inject.Inject

class NavNavigationImpl @Inject constructor(
    private val fragmentNav: NavigationFragment
) : INavNavigation {


    override fun goTo(destination: Destination) {
        var fragment: BaseFragment? = null
        when (destination) {
            Destination.TopThreeGamesScreen -> {
                fragment = TopThreeGamesFragment.getInstance()
            }
        }
        goToReplaceFragment(fragment)
    }


    private fun goToReplaceFragment(fragment: Fragment?) {
        fragment?.let {
            fragmentNav.childFragmentManager.beginTransaction()
                .replace(R.id.frmNavField, fragment)
                .commit()
        }
    }

}
