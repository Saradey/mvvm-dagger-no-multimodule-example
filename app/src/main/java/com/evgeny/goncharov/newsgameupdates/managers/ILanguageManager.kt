package com.evgeny.goncharov.newsgameupdates.managers

import java.util.*

interface ILanguageManager {

    fun getAppLanguage(): Locale

    fun getAppLanguageCode(): String

}