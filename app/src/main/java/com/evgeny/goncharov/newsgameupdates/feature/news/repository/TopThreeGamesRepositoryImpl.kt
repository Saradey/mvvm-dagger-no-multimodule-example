package com.evgeny.goncharov.newsgameupdates.feature.news.repository

import com.evgeny.goncharov.newsgameupdates.feature.news.db.TopThreeDAO
import com.evgeny.goncharov.newsgameupdates.feature.news.model.response.TopThreeResponse
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import com.evgeny.goncharov.newsgameupdates.feature.news.rest.TopThreeApi
import com.evgeny.goncharov.newsgameupdates.utils.DateFormatter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class TopThreeGamesRepositoryImpl @Inject constructor(
    private val api: TopThreeApi,
    private val dateFormatter: DateFormatter,
    private val daoTopThree: TopThreeDAO
) : ITopThreeGamesRepository {


    override suspend fun loadFromInternet(
        request: Map<String, String>
    ) {
        val responseModel = api.getTopThreeGameAsync(
            request
        ).await()
        saveResponseTopThree(responseModel.data)
    }


    override suspend fun loadFromDataBase(): List<TopThreeModelView> {
        return suspendCoroutine { continuation ->
            continuation.resume(daoTopThree.getAll().map { response ->
                TopThreeModelView(
                    nameGame = response.nameGame ?: "-",
                    url = response.url ?: "-",
                    date = dateFormat(response.dateUpdate) ?: "-"
                )
            })
        }
    }


    private fun dateFormat(date: Date?): String? {
        date?.let {
            return dateFormatter.sdfDateSpaceMonthFullSpaceYear.format(date)
        }
        return null
    }


    private suspend fun saveResponseTopThree(models: List<TopThreeResponse>?) =
        withContext(Dispatchers.IO) {
            daoTopThree.clearAndInsert(models ?: listOf())
        }
}