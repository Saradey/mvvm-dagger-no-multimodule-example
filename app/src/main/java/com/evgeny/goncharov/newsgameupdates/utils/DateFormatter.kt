package com.evgeny.goncharov.newsgameupdates.utils

import com.evgeny.goncharov.newsgameupdates.managers.ILanguageManager
import java.text.SimpleDateFormat

class DateFormatter(
    private val languageManager: ILanguageManager
) {
    val sdfDateSpaceMonthFullSpaceYear =
        SimpleDateFormat(FORMAT_DATE_SPACE_MONTH_FULL_SPACE_YEAR, languageManager.getAppLanguage())
    val sdfFullAllInformationDate = SimpleDateFormat(
        FORMAT_FULL_YEAR_MONTH_DAY_T_HOUR_MINUTES_SECONDS_MS_TIMEZONE,
        languageManager.getAppLanguage()
    )
}