package com.evgeny.goncharov.newsgameupdates.di.components

import com.evgeny.goncharov.newsgameupdates.App
import com.evgeny.goncharov.newsgameupdates.MainActivity
import com.evgeny.goncharov.newsgameupdates.db.type.converters.TypeConverterDate
import com.evgeny.goncharov.newsgameupdates.di.modules.app.*
import com.evgeny.goncharov.newsgameupdates.di.scopes.AppScope
import dagger.BindsInstance
import dagger.Component


@AppScope
@Component(
    modules = [
        AppBindsModule::class,
        CreateActivitySubcomponentModule::class,
        AppUtilsModule::class,
        DatabaseModule::class,
        RestModule::class
    ]
)
interface AppComponent {

    fun inject(activity: MainActivity)

    fun inject(typeConverter: TypeConverterDate)

    @Component.Factory
    interface Factory {
        fun plus(@BindsInstance app: App): AppComponent
    }

}