package com.evgeny.goncharov.newsgameupdates.feature.news.ui.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import kotlinx.android.synthetic.main.holder_viewpager_top_game.view.*

class TopThreeHolder(view: View) : RecyclerView.ViewHolder(view) {


    fun bind(topThreeModelView: TopThreeModelView) {
        itemView.txvTitleNameGame.text = topThreeModelView.nameGame
        itemView.txvDateUpdateNews.text =
            itemView.resources.getString(R.string.update_title, topThreeModelView.date)
        itemView.cnlContainerChild.clipToOutline = true
        Glide
            .with(itemView)
            .load(topThreeModelView.url)
            .into(itemView.imvPosterGame)
    }


}