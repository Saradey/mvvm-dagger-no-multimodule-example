package com.evgeny.goncharov.newsgameupdates.feature.nav.di.modules

import com.evgeny.goncharov.newsgameupdates.feature.news.di.TopThreeGamesSubcomponent
import dagger.Module

@Module(subcomponents = [TopThreeGamesSubcomponent::class])
interface NavigationModuleCreateSubcomponents