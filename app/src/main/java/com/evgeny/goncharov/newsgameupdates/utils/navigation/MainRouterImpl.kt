package com.evgeny.goncharov.newsgameupdates.utils.navigation

import javax.inject.Inject


class MainRouterImpl @Inject constructor(
    private val navigator: IMainNavigator
) : IMainRouter {


    override fun startSplashScreen() {
        navigator.goTo(Destination.SplashScreen)
    }


    override fun goToTheNavigationBotttomScreen() {
        navigator.goTo(Destination.NavigationBottomScreen)
    }

}