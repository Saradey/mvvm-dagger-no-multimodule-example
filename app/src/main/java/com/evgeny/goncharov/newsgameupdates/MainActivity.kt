package com.evgeny.goncharov.newsgameupdates

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.evgeny.goncharov.newsgameupdates.di.components.ActivitySubcomponent
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainNavigator
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainRouter
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    companion object {
        lateinit var activityComponent: ActivitySubcomponent
    }

    @Inject
    lateinit var factory: ActivitySubcomponent.Factory

    @Inject
    lateinit var router: IMainRouter

    @Inject
    lateinit var navigator: IMainNavigator

    private lateinit var mainLifecycle: MainActivityLifecycle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.appComponent.inject(this)

        activityComponent = factory.plus(
            mainActivity = this
        )
        mainLifecycle = MainActivityLifecycle(this, navigator)
        lifecycle.addObserver(mainLifecycle)

        navigator.attachActivity(this)
        if (savedInstanceState == null) {
            router.startSplashScreen()
        }
    }


}