package com.evgeny.goncharov.newsgameupdates.utils.navigation

import androidx.fragment.app.Fragment
import com.evgeny.goncharov.newsgameupdates.MainActivity
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.feature.splash.SplashScreenFragment
import javax.inject.Inject

class MainNavigatorImpl @Inject constructor() : IMainNavigator {

    private var activity: MainActivity? = null

    override fun attachActivity(mainActivity: MainActivity) {
        this.activity = mainActivity
    }

    override fun detachActivity() {
        this.activity = null
    }


    override fun goTo(des: Destination) {
        var fragment: Fragment? = null
        when (des) {
            Destination.SplashScreen -> fragment = SplashScreenFragment()
            Destination.NavigationBottomScreen -> fragment =
                NavigationFragment()
        }
        goToReplaceFragment(fragment)
    }


    private fun goToReplaceFragment(fragment: Fragment?) {
        fragment?.let {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frmFiledApplication, fragment)
                ?.commit()
        }
    }

}