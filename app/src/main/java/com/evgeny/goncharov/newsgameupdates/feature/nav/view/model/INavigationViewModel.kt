package com.evgeny.goncharov.newsgameupdates.feature.nav.view.model

import androidx.lifecycle.MutableLiveData

interface INavigationViewModel {

    fun clickNavBottomTopThree()

    fun clickNavBottomLogin()

    fun clickNavBottomSettings()

    fun initTopThreeFragment()

}