package com.evgeny.goncharov.newsgameupdates.feature.news.db

import androidx.room.*
import com.evgeny.goncharov.newsgameupdates.feature.news.model.response.TopThreeResponse

@Dao
abstract class TopThreeDAO {

    @Query("DELETE FROM TopThreeResponse")
    abstract fun deleteAll()

    @Insert
    abstract fun insert(model: List<TopThreeResponse>)

    @Query("SELECT * FROM TopThreeResponse")
    abstract fun getAll(): List<TopThreeResponse>


    @Transaction
    open fun clearAndInsert(model: List<TopThreeResponse>) {
        deleteAll()
        insert(model)
    }
}