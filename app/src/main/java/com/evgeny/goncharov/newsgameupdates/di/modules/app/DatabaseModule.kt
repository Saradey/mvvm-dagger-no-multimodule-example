package com.evgeny.goncharov.newsgameupdates.di.modules.app

import android.content.Context
import androidx.room.Room
import com.evgeny.goncharov.newsgameupdates.db.AppDatabase
import com.evgeny.goncharov.newsgameupdates.di.scopes.AppScope
import com.evgeny.goncharov.newsgameupdates.feature.news.db.TopThreeDAO
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_APPLICATION_TAG
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class DatabaseModule {

    @AppScope
    @Provides
    fun provideDatabase(@Named(INJECT_APPLICATION_TAG) context: Context):
            AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, "pisa").build()

    @Provides
    fun provideTopThreeDAO(database: AppDatabase): TopThreeDAO = database.createTopThreeDAO()

}