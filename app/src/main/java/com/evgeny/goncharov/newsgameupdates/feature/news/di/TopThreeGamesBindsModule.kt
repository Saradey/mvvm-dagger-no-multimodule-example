package com.evgeny.goncharov.newsgameupdates.feature.news.di

import com.evgeny.goncharov.newsgameupdates.feature.news.interactor.ITopThreeGamesInteractor
import com.evgeny.goncharov.newsgameupdates.feature.news.interactor.TopThreeGamesInteractorImpl
import com.evgeny.goncharov.newsgameupdates.feature.news.repository.ITopThreeGamesRepository
import com.evgeny.goncharov.newsgameupdates.feature.news.repository.TopThreeGamesRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface TopThreeGamesBindsModule {

    @Binds
    fun bindTopThreeGamesInteractor(interactor: TopThreeGamesInteractorImpl): ITopThreeGamesInteractor

    @Binds
    fun bindTopThreeGamesRepository(repository: TopThreeGamesRepositoryImpl): ITopThreeGamesRepository

}