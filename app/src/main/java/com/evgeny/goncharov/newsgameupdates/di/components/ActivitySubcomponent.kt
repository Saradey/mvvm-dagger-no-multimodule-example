package com.evgeny.goncharov.newsgameupdates.di.components

import com.evgeny.goncharov.newsgameupdates.MainActivity
import com.evgeny.goncharov.newsgameupdates.di.modules.activity.ActivityBindsModule
import com.evgeny.goncharov.newsgameupdates.di.modules.activity.CreateFragmentsSubcomponentModule
import com.evgeny.goncharov.newsgameupdates.di.scopes.ActivityScope
import com.evgeny.goncharov.newsgameupdates.feature.nav.di.FragmentNavigationSubcomponent
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.feature.splash.SplashScreenFragment
import dagger.BindsInstance
import dagger.Subcomponent


@ActivityScope
@Subcomponent(
    modules = [
        ActivityBindsModule::class,
        CreateFragmentsSubcomponentModule::class
    ]
)
interface ActivitySubcomponent {

    //fragments
    fun inject(fragment: SplashScreenFragment)

    fun inject(fragment: NavigationFragment)


    @Subcomponent.Factory
    interface Factory {

        fun plus(
            @BindsInstance mainActivity: MainActivity
        ): ActivitySubcomponent

    }

}