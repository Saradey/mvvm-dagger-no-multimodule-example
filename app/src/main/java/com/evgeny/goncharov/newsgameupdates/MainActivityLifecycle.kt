package com.evgeny.goncharov.newsgameupdates

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.evgeny.goncharov.newsgameupdates.utils.navigation.IMainNavigator

class MainActivityLifecycle(
    private val activity: MainActivity,
    private val navigator: IMainNavigator
) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        navigator.attachActivity(activity)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        navigator.detachActivity()
    }

}