package com.evgeny.goncharov.newsgameupdates.feature.news.interactor

import androidx.lifecycle.MutableLiveData
import com.evgeny.goncharov.newsgameupdates.feature.news.model.request.TopThreeRequest
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import com.evgeny.goncharov.newsgameupdates.feature.news.repository.ITopThreeGamesRepository
import com.evgeny.goncharov.newsgameupdates.managers.ILanguageManager
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_IO_SCOPE_TAG
import com.evgeny.goncharov.newsgameupdates.utils.UiEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class TopThreeGamesInteractorImpl @Inject constructor(
    private val repository: ITopThreeGamesRepository,
    @Named(INJECT_IO_SCOPE_TAG) private val ioScope: CoroutineScope,
    private val languageManager: ILanguageManager,
    private val uiEventsLiveData: MutableLiveData<UiEvent>
) : ITopThreeGamesInteractor {


    override suspend fun initViewState(): List<TopThreeModelView>? {
        uiEventsLiveData.postValue(UiEvent.EventShowLoader)
        ioScope.launch(CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
        }) {
            repository.loadFromInternet(TopThreeRequest(languageManager.getAppLanguageCode()).createQueryMap())
        }.join()

        return suspendCoroutine { continuation ->
            ioScope.launch(CoroutineExceptionHandler { _, throwable ->
                throwable.printStackTrace()
                uiEventsLiveData.postValue(UiEvent.EventShowErrorSomethingWrong)
            }) {
                val model = repository.loadFromDataBase()
                if (model.isNotEmpty()) {
                    continuation.resume(model)
                } else {
                    uiEventsLiveData.postValue(UiEvent.EventShowErrorSomethingWrong)
                }
            }
            uiEventsLiveData.postValue(UiEvent.EventHideLoader)
        }
    }


}