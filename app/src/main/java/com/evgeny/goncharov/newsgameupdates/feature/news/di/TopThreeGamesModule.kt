package com.evgeny.goncharov.newsgameupdates.feature.news.di

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.evgeny.goncharov.newsgameupdates.di.scopes.FragmentScope
import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView
import com.evgeny.goncharov.newsgameupdates.feature.news.rest.TopThreeApi
import com.evgeny.goncharov.newsgameupdates.feature.news.ui.TopThreeGamesFragment
import com.evgeny.goncharov.newsgameupdates.feature.news.view.model.ITopThreeGamesViewModel
import com.evgeny.goncharov.newsgameupdates.feature.news.view.model.TopThreeGamesViewModel
import com.evgeny.goncharov.newsgameupdates.utils.UiEvent
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_IO_SCOPE_TAG
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_MAIN_SCOPE_TAG
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import javax.inject.Named


@Module
class TopThreeGamesModule {

    @Provides
    fun provideTopThreeGamesViewModel(
        fragment: TopThreeGamesFragment
    ): ITopThreeGamesViewModel =
        ViewModelProviders.of(fragment).get(TopThreeGamesViewModel::class.java)


    @Provides
    @FragmentScope
    fun provideApiTopThree(retrofit: Retrofit): TopThreeApi =
        retrofit.create(TopThreeApi::class.java)


    @Provides
    @FragmentScope
    @Named(INJECT_MAIN_SCOPE_TAG)
    fun provideMainScope(): CoroutineScope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    @Provides
    @FragmentScope
    @Named(INJECT_IO_SCOPE_TAG)
    fun provideIoScope(): CoroutineScope = CoroutineScope(Dispatchers.IO + SupervisorJob())

    @Provides
    @FragmentScope
    fun provideLiveDataFlowModel():
            MutableLiveData<List<TopThreeModelView>> = MutableLiveData()

    @Provides
    @FragmentScope
    fun provideLiveDataEvents(): MutableLiveData<UiEvent> = MutableLiveData()

}