package com.evgeny.goncharov.newsgameupdates.base

interface IBaseStateErrorView {

    fun showErrorSomethingWrong()

    fun hideErrorSomethingWrong()

}