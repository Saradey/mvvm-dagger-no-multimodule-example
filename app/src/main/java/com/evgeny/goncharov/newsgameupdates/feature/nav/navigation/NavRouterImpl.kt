package com.evgeny.goncharov.newsgameupdates.feature.nav.navigation

import com.evgeny.goncharov.newsgameupdates.utils.navigation.Destination
import javax.inject.Inject

class NavRouterImpl @Inject constructor(
    private val navigation: INavNavigation
) : INavRouter {

    override fun goToTheTopThree() {
        navigation.goTo(Destination.TopThreeGamesScreen)
    }

}