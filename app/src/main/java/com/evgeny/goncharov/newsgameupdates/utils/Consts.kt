package com.evgeny.goncharov.newsgameupdates.utils

//TAG INJECT
const val INJECT_APPLICATION_TAG = "ApplicationContext"
const val INJECT_ACTIVITY_TAG = "MainActivityContext"
const val INJECT_IO_SCOPE_TAG = "IoScope"
const val INJECT_MAIN_SCOPE_TAG = "MainScope"

//URL
//свой мокнутый бекэнд
const val BASE_URL = "http://10.0.2.2:8080/"


//default api key
const val API_KEY_VALUE = "dbhasn34823y47dgfsvfjh3267easdasd312"


//Query name param
const val API_KEY = "apiKey"
const val LANG = "lang"


//pattern date format
const val FORMAT_DATE_SPACE_MONTH_FULL_SPACE_YEAR = "dd MMMM yyyy"
const val FORMAT_FULL_YEAR_MONTH_DAY_T_HOUR_MINUTES_SECONDS_MS_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"

//dataBase version
const val DATABASE_VERSION_1 = 1