package com.evgeny.goncharov.newsgameupdates.feature.news.rest

import com.evgeny.goncharov.newsgameupdates.feature.news.model.response.TopThreeResponse
import com.evgeny.goncharov.newsgameupdates.repository.BaseResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface TopThreeApi {

    @GET("games/news/get/top/three")
    fun getTopThreeGameAsync(@QueryMap request: Map<String, String>):
            Deferred<BaseResponse<List<TopThreeResponse>>>


}