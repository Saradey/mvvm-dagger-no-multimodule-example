package com.evgeny.goncharov.newsgameupdates.feature.nav.di.modules

import com.evgeny.goncharov.newsgameupdates.di.scopes.NavigationFragmentScope
import com.evgeny.goncharov.newsgameupdates.feature.nav.navigation.INavNavigation
import com.evgeny.goncharov.newsgameupdates.feature.nav.navigation.INavRouter
import com.evgeny.goncharov.newsgameupdates.feature.nav.navigation.NavNavigationImpl
import com.evgeny.goncharov.newsgameupdates.feature.nav.navigation.NavRouterImpl
import dagger.Binds
import dagger.Module

@Module
interface NavigationBindsModule {

    @Binds
    @NavigationFragmentScope
    fun bindsNavigator(nav: NavNavigationImpl): INavNavigation


    @Binds
    @NavigationFragmentScope
    fun bindsNavRouter(router: NavRouterImpl): INavRouter

}