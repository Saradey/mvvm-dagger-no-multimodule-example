package com.evgeny.goncharov.newsgameupdates.feature.news.interactor

import com.evgeny.goncharov.newsgameupdates.feature.news.model.view.TopThreeModelView

interface ITopThreeGamesInteractor {

    suspend fun initViewState(): List<TopThreeModelView>?

}