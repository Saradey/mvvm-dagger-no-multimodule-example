package com.evgeny.goncharov.newsgameupdates.managers.impl

import com.evgeny.goncharov.newsgameupdates.managers.ILanguageManager
import java.util.*
import javax.inject.Inject

class LanguageManagerImpl @Inject constructor() : ILanguageManager {

    private var codeLanguage = Locale.getDefault().country

    override fun getAppLanguage(): Locale {
        return Locale.getDefault()
    }


    override fun getAppLanguageCode(): String {
        return codeLanguage
    }
}