package com.evgeny.goncharov.newsgameupdates.base

import android.view.View

interface IBaseView {

    fun attach(view: View?)

    fun init()

    fun injectView()

    fun showProgress()

    fun hideProgress()

    fun initUi()

}