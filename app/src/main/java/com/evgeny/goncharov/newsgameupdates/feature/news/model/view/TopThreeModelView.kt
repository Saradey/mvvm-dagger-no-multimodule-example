package com.evgeny.goncharov.newsgameupdates.feature.news.model.view

data class TopThreeModelView(
    val nameGame: String,
    val url: String,
    val date: String
)