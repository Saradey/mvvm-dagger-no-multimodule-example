package com.evgeny.goncharov.newsgameupdates.di.modules.activity

import com.evgeny.goncharov.newsgameupdates.feature.nav.di.FragmentNavigationSubcomponent
import dagger.Module

@Module(
    subcomponents = [FragmentNavigationSubcomponent::class]
)
interface CreateFragmentsSubcomponentModule