package com.evgeny.goncharov.newsgameupdates.feature.news.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.evgeny.goncharov.newsgameupdates.R
import com.evgeny.goncharov.newsgameupdates.base.BaseFragment
import com.evgeny.goncharov.newsgameupdates.feature.nav.ui.NavigationFragment
import com.evgeny.goncharov.newsgameupdates.feature.news.di.TopThreeGamesSubcomponent
import javax.inject.Inject

class TopThreeGamesFragment : BaseFragment() {

    @Inject
    lateinit var factory: TopThreeGamesSubcomponent.Factory

    lateinit var view: ITopThreeGamesView

    companion object {
        lateinit var component: TopThreeGamesSubcomponent

        fun getInstance(): TopThreeGamesFragment {
            return TopThreeGamesFragment()
        }
    }


    override fun getLayoutId(): Int = R.layout.fragment_top_three_games


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NavigationFragment.component.inject(this)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        createView(view)
        if (savedInstanceState == null) {
            component = factory.plus(this, this.view)
        }
        this.view.injectView()
        return view
    }


    override fun createView(view: View?) {
        this.view = TopThreeGamesViewImpl()
        this.view.attach(view)
    }


    override fun init() {
        view.init()
    }


}