package com.evgeny.goncharov.newsgameupdates.di.modules.app

import android.content.Context
import com.evgeny.goncharov.newsgameupdates.BuildConfig
import com.evgeny.goncharov.newsgameupdates.di.scopes.AppScope
import com.evgeny.goncharov.newsgameupdates.utils.BASE_URL
import com.evgeny.goncharov.newsgameupdates.utils.INJECT_APPLICATION_TAG
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
class RestModule {

    @Provides
    @AppScope
    fun provideLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(
        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    )


    @Provides
    @AppScope
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        @Named(INJECT_APPLICATION_TAG) context: Context
    ): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(ChuckInterceptor(context))
            .addInterceptor(loggingInterceptor)
            .build()


    @Provides
    @AppScope
    fun provideRetrofit(okHttp: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttp)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

}